<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<html>

<head>
  <title>Поля игроков</title>
</head>

<body>
<br>${player1name}
<table border='1'>
  <c:forEach var="row" items="${player1field.getField()}">
    <tr>
    <c:forEach var="col" items="${row}">
      <c:choose>
        <c:when test="${col.getShip()!=null}">
          <td style='width:20px;height:20px;background-color:red'></td>
        </c:when>
        <c:otherwise>
          <td style='width:20px;height:20px;background-color:white'></td>
        </c:otherwise>
      </c:choose>
    </c:forEach>
    </tr>
  </c:forEach>
</table>
<hr>
<br>${player2name}
<table border='1'>
  <c:forEach var="row" items="${player2field.getField()}">
    <tr>
      <c:forEach var="col" items="${row}">
        <c:choose>
          <c:when test="${col.getShip()!=null}">
            <td style='width:20px;height:20px;background-color:red'></td>
          </c:when>
          <c:otherwise>
            <td style='width:20px;height:20px;background-color:white'></td>
          </c:otherwise>
        </c:choose>
      </c:forEach>
    </tr>
  </c:forEach>
</table>
</body>
</html>